#include <stdio.h>
#include <immintrin.h>
#include <math.h>
#include <errno.h>
#include <CImg.h>

using namespace cimg_library;

// Data type for image components
// DONE: Change this type according to your group assignment
typedef double data_t;
typedef __m128d packet_t;

#define REPETITIONS 20

#define ITEMS_PER_PACKET (sizeof(packet_t)/sizeof(data_t))

#define exLoad1(x) _mm_load1_pd(x)
#define exLoad(x) _mm_loadu_pd(x)

#define exAdd(a,b) _mm_add_pd(a,b)
#define exSub(a,b) _mm_sub_pd(a,b)
#define exMul(a,b) _mm_mul_pd(a,b)
#define exDiv(a,b) _mm_div_pd(a,b)

#define exStore(addr, value) *(packet_t *)addr = value;



const char* SOURCE_IMG      = "bailarina.bmp";
const char* DESTINATION_IMG = "bailarina2.bmp";
const char* FILTER_IMG = "background_V.bmp";



int main()
{
    // Open file and object initialization
	CImg<data_t> srcImage(SOURCE_IMG);
	CImg<data_t> filtImage(FILTER_IMG); //Initialize filter image 

	data_t *pRsrc, *pGsrc, *pBsrc; // Pointers to the R, G and B components of the source image
	data_t *pRfilt, *pGfilt, *pBfilt; // Pointers to the R, G and B components of the filter image
	data_t *pRdest, *pGdest, *pBdest; // Pointers to the R, G and B components of the destination image
	data_t *pDstImage; // Pointer to the new image pixels
	data_t *pDstImgEx;
	uint width, height; // Width and height of the image
	uint nComp; // Number of image components


	/***************************************************
	 * DONE: Variables initialization.
	 *   - Prepare variables for the algorithm
	 *   - This is not included in the benchmark time
	 */

	srcImage.display(); // Displays the source image
	width  = srcImage.width(); // Getting information from the source image
	height = srcImage.height();
	nComp  = srcImage.spectrum(); // source image number of components
				// Common values for spectrum (number of image components):
				//  B&W images = 1
				//	Normal color images = 3 (RGB)
				//  Special color images = 4 (RGB and alpha/transparency channel)

	//Check if filter and source have the same dimensions
	if (!(width == (uint)filtImage.width() && height == (uint)filtImage.height() && nComp == (uint)filtImage.spectrum()))
	{
		perror("Dimension do not match");
		exit(-3);
	}

	filtImage.display();
	
	pDstImgEx = (data_t *) _mm_malloc (((width * height * nComp)/ITEMS_PER_PACKET) * sizeof(packet_t), sizeof(packet_t));
	if (pDstImgEx == NULL)
	{
		perror("Allocating destination image");
		exit(-2);
	}

	// Pointers to the component arrays of the source image
	pRsrc = srcImage.data(); // pRcomp points to the R component array
	pGsrc = pRsrc + height * width; // pGcomp points to the G component array
	pBsrc = pGsrc + height * width; // pBcomp points to B component array
	//Pointers to the component arrays of the filter image
	pRfilt = filtImage.data(); // pRcomp points to the R component array
	pGfilt = pRfilt + height * width; // pGcomp points to the G component array
	pBfilt = pGfilt + height * width; // pBcomp points to B component array
	// Pointers to the RGB arrays of the destination image
	pRdest = pDstImgEx;
	pGdest = pRdest + height * width;
	pBdest = pGdest + height * width;
    

    register packet_t pSource;
	register packet_t pFilter;
	register packet_t pVar1;
	register packet_t pVar2;
	
    // Prepare constant packets
    const data_t two55n[ITEMS_PER_PACKET] = {(data_t)255, (data_t)255};
    const packet_t two55v = exLoad1(two55n); // two55v = {255, 255}
    const data_t twoN[ITEMS_PER_PACKET] = {(data_t)2, (data_t)2};
    const packet_t twoV = exLoad1(twoN);


	/***********************************************
	 * DONE: Algorithm start.
	 *   - Measure initial time
	 */

	struct timespec tStart, tEnd;
    double dElapsedTimeS;
    // Start time measurement
	printf("\nStarting algorithm...\n");
    if(clock_gettime(CLOCK_REALTIME, &tStart) != 0)
	{
		perror("Error getting clock information");
		exit(EXIT_FAILURE);
	}

	/************************************************
	 * DONE: Algorithm.
	 * In this example, the algorithm is a components swap
	 *
	 * TO BE REPLACED BY YOUR ALGORITHM
	 */
	
	for (int j = 0; j < REPETITIONS; j++)
	{
		for (uint i = 0; i < width * height; i+=ITEMS_PER_PACKET)
		{
			// Red
            pSource = exLoad(pRsrc+i);
            pFilter = exLoad(pRfilt+i);

			pVar1 = exMul(twoV, pSource);
			pVar1 = exDiv(pVar1, two55v);
			pVar2 = exSub(two55v, pFilter);
			pVar2 = exMul(pVar1, pVar2);
			pVar2 = exAdd(pFilter, pVar2);
			pVar1 = exDiv(pFilter, two55v);
			
			*(packet_t *)(pRdest+i) = exMul(pVar1, pVar2);
            
            // Green
            pSource = exLoad(pGsrc+i);
            pFilter = exLoad(pGfilt+i);

            pVar1 = exMul(twoV, pSource);
			pVar1 = exDiv(pVar1, two55v);
			pVar2 = exSub(two55v, pFilter);
			pVar2 = exMul(pVar1, pVar2);
			pVar2 = exAdd(pFilter, pVar2);
			pVar1 = exDiv(pFilter, two55v);
			
			*(packet_t *)(pGdest+i) = exMul(pVar1, pVar2);
            
			// Blue
            pSource = exLoad(pBsrc+i);
            pFilter = exLoad(pBfilt+i);

            pVar1 = exMul(twoV, pSource);
			pVar1 = exDiv(pVar1, two55v);
			pVar2 = exSub(two55v, pFilter);
			pVar2 = exMul(pVar1, pVar2);
			pVar2 = exAdd(pFilter, pVar2);
			pVar1 = exDiv(pFilter, two55v);
			
			*(packet_t *)(pBdest+i) = exMul(pVar1, pVar2);
		}
	}
    
	pDstImage = pDstImgEx;

	/***********************************************
	 * DONE: End of the algorithm.
	 *   - Measure the end time
	 *   - Calculate the elapsed time
	 */

	// Finish time measurement
    if(clock_gettime(CLOCK_REALTIME, &tEnd) != 0)
	{
		perror("Error getting clock information");
		exit(EXIT_FAILURE);
	}

    // Compute the elapsed time, in seconds
    dElapsedTimeS = (tEnd.tv_sec - tStart.tv_sec);
    dElapsedTimeS += (tEnd.tv_nsec - tStart.tv_nsec) / 1e+9;

	printf("Done.\nElapsed time: %f seconds\n\n.", dElapsedTimeS);
	// Create a new image object with the calculated pixels
	// In case of normal color images use nComp=3,
	// In case of B/W images use nComp=1.
	
	CImg<data_t> dstImage(pDstImage, width, height, 1, nComp);
	// Store destination image in disk
	dstImage.save(DESTINATION_IMG); 

	// Display destination image
	dstImage.display();

    // Free memory
	_mm_free(pDstImgEx);
	return 0;
}